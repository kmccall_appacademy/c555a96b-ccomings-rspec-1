def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word, num=2)
  str = "#{word} " * num
  str[0...-1]
end

def start_of_word(word, num_of_letters)
  word[0...num_of_letters]
end

def first_word(words)
  words.split.first
end

def titleize(movie_title)
  movie_array = movie_title.split
  small_words = ['the','over','and']
  movie_array.each do |word|
    word.capitalize! unless small_words.include?(word)
  end
  movie_array[0].capitalize!
  movie_array.join(' ')
end
