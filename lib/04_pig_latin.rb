def translate(word)
  latin_words = []
  word.split.each do |w|
    latin_words << latinize(w)
  end
  latin_words.join(' ')
end

def latinize(single_word)
  vowels = 'aieou'
  latin_word = ''
  single_word.each_char.with_index do |ch, i|
    if single_word[i..(i+1)] == 'qu'
      latin_word << single_word[(i+2)..-1] + single_word[0..(i+1)] + 'ay'
      break
    end
    if vowels.include?(ch)
      latin_word << single_word[i..-1] + single_word[0...i] + 'ay'
      break
    end
  end
  latin_word
end
