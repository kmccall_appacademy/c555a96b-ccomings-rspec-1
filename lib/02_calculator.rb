def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array_of_numbers)
  return 0 if array_of_numbers == []
  array_of_numbers.reduce(:+)
end

def multiply(array_of_numbers)
  array_of_numbers.reduce(:*)
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  if num.zero?
    1
  else (1..num).reduce(:*)
  end
end
